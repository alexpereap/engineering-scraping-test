from .Scraper import Scraper

class SimpleAuthTable(Scraper):
    """
    Class to demonstrate content scrap from example 4 from the test
    Since the providen url was not working I used the following url
    http://laravel-simple-auth-example.alexpereap.com/
    username: alexp password: Williams
    which implements a simple test auth login form using a csrf token
    After login, the get_table method can be used to get the contents from an HTML table
    """

    page_url  = 'http://laravel-simple-auth-example.alexpereap.com/'
    login_url = 'http://laravel-simple-auth-example.alexpereap.com/login'
    payload   = {
          "username" : "alexp",
          "password" : "Williams",
          "token"    : "_token"
        }

    def __init__(self, *args, **kwargs):
        super(SimpleAuthTable, self).__init__(*args, **kwargs)
        self.soup = self.get_soup_object_with_auth(self.page_url, self.login_url, self.payload )

    def get_table(self):
        """get the table contents after auth"""
        table_data = []

        table = self.soup.find('table',class_='table')
        table_body = table.find('tbody')
        rows = table_body.find_all('tr')

        for row in rows:
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
            table_data.append([ele for ele in cols if ele]) # Get rid of empty values

        return table_data