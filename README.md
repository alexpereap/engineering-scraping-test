# Engineering-scraping-test
This is the engineering scraping test, thank you for downloading this repo and taking the time to review the source code.

## Problems Chosen:

 - Extract the title and body of a resource: Those are the basics of web scraping and it was good place to start to get used to the implemented techniques for the content parsing and DOM navigation using python. I was able to get the page title and the article contents from the proposed blog website on the example 2 https://www.theguardian.com/politics/2018/aug/19/brexit-tory-mps-warn-of-entryism-threat-from-leave-eu-supporters stripping all of the html tags, leaving only the human readable content. 
 - Extract images from a content item: I wanted to go a little bit further by getting the source attributes from the images tags included on the main article tag from the same blog on the example 2. I was able to extract that data which I stored it on a python list.
 - Orchestrate requests to remote servers -> Authentication: This seemed like a very interesting challenge to take, to emulate a login form! I tried to use the proposed website on the example 4 https://learning.econsultancy.com/module/2 however after registering, when accessing the url a message shown telling I didn't have enough rights to get the content. Given that, I used a test site I did a while ago, that implements CSRF token basic authentication: http://laravel-simple-auth-example.alexpereap.com/ (username: alexp password: Williams) At the end I was able to emulate the login and get the contents from the after-login url.
 - Ingest resources other than article-like HTML pages, i.e:  HTML Data Tables: I wanted to see if I could store an html table contents on a python list, actually I was able to! From the auth site I captured the data from a table and stored it in the format of a python list, more details on that can be read below in the project run section.
 -  Handle responses of remote servers: I wanted to see how could I handle the different server responses, what I found was that the python request library does almost all of the job, automatically handling redirect requests. However, I validated any time that something different from a status 200 was sent as a response on the scripts using http requests.
 
## Installation

 1. Enable virtual env:** from the console run: source bin/activate
 2. Install requirements:** from the console run: pip3 install -r requirements.txt 

## Project run
From the console run: python3 src/do-scraping.py
This will generate two files: 

 1. blog_example.txt: This file is the result of scraping the website: https://www.theguardian.com/politics/2018/aug/19/brexit-tory-mps-warn-of-entryism-threat-from-leave-eu-supporters It will show the page title, the article contents and a list of the article images.
 2. auth_table_example.txt: This file is the result of scraping the following url: http://laravel-simple-auth-example.alexpereap.com/ this site is a simple login form implemented in laravel, which uses CSRF token security. After logging in,  a table with the formula 1 championship drivers standing is shown, this table contest is stored in the file with the python list format. (username: alexp password: Williams)

## Test Suite
To run tests execute in the console: python3 src/tests.py
Each test description is commented in the file source code

## Code Summary
All of the main scripts are on the src/ folder.
The main script is: **/do-scraping.py** which instantiates the **BlogExample** and **SimpleAuthTable** classes and write the web scraping results on the above mentioned .txt files.
These classes are stored on the classes/ folder, they both have dedicated methods to scrap the required content and they both extend the **/Scraper.py** Class, which is main class that contains methods for scraping content with or without the need of authentication.

All of the code is commented for more in deep details!
## Thank you!
