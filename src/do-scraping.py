from Classes.BlogExample import BlogExample
from Classes.SimpleAuthTable import SimpleAuthTable

import os
path = os.path.dirname(os.path.abspath(__file__))

# Writes to blog_example.txt the results from the blog example
# The title, contents, and article images are scrapped from the url
# url: https://www.theguardian.com/politics/2018/aug/19/brexit-tory-mps-warn-of-entryism-threat-from-leave-eu-supporters
blog_example = BlogExample()
f = open(path + "/../blog_example.txt", "w+")
f.write("Page title: " + blog_example.get_title() )
f.write("\n\n")
f.write("Contents: " + blog_example.get_article_contents() )
f.write("\n\n")
f.write("Images: " + str(blog_example.get_article_images()) )
f.close()

# It logins to a website and then extracts the contents of a HTML table
# Writes the table contents in list format to the auth_table_example.txt file
# url: http://laravel-simple-auth-example.alexpereap.com/
auth_table = SimpleAuthTable()
f = open(path + "/../auth_table_example.txt", "w+")
f.write("Table contents: \n")
f.write( str(auth_table.get_table()) )
f.close()

# Done!
print("Scrapping Done!")