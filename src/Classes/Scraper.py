from bs4 import BeautifulSoup
import requests

class Scraper():
    """Scraper class to handle requests / responses to websites"""
    def __init__(self, *args, **kwargs):
        super(Scraper, self).__init__(*args, **kwargs)
    
    def get_soup_object(self, url):
        """
        Gets the beautifulsoup object for a given url
        Parameters:
        url (str): Webpage url

        Returns:
        BeautifulSoup object: If successfull response to the request
        False: If un successfull response to the request    
        """

        #validates url is string
        if( not isinstance(url, str) ):
            return False

        r = requests.get(url)

        if( r.status_code != 200 ):
            return False

        return BeautifulSoup(r.content, 'html.parser')
    
    def get_soup_object_with_auth(self, page_url, login_url, payload ):
        """
        Gets the beautifulsoup object for a given url that requires authentication
        Parameters:
        page_url (str):  Url that shows up the login form
        login_url (str): Url used for the login form (action attr) to perform the authentication
        payload (dict):  Dict with the following format
        {
            username : <str username>,
            password : <str password>,
            token    : <str csrf token input name on the login form >
        }
        Returns:
        BeautifulSoup object: If successfull response to the request
        False: If un successfull response to the request    
        """
        session_requests = requests.session()

        #validates url is string
        if( not isinstance(page_url, str) ):
            return False

        r = session_requests.get(page_url)

        # Login form responded 
        if( r.status_code != 200 ):
            # session_requests.close()
            return False

        soup = BeautifulSoup(r.content, 'html.parser')
        token = soup.find('input',{'name': payload['token'] }).get('value')

        # builds the auth data payload
        auth_data = {
            "username"         : payload['username'],
            "password"         : payload['password'],
            payload['token']   : token
        }

        # do a POST request to the login form action
        result_with_auth = session_requests.post(
            login_url, 
            data = auth_data, 
            headers = dict(referer=login_url)
        )

        session_requests.close()

        # The login wasn´t successful if the response url is the same than the login url
        if( result_with_auth.status_code == 200 ):
            if( result_with_auth.url == login_url ):
                return False
        else:
            return False
        
        # returns the contect from the redirect from the successful auth 
        return BeautifulSoup(result_with_auth.content, 'html.parser')