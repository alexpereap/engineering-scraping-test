import unittest
import os

from Classes.Scraper import Scraper
from Classes.BlogExample import BlogExample
from Classes.SimpleAuthTable import SimpleAuthTable

class TestScrapping(unittest.TestCase):

    path = os.path.dirname(os.path.abspath(__file__))

    def test_scraper(self):
        sc = Scraper()

        # get_soup_object tests - function used by blog exampñe
        self.assertTrue( sc.get_soup_object('https://www.python.org/'), 'expects true, page found' )

        self.assertFalse( sc.get_soup_object('http://laravel-simple-auth-example.alexpereap.com/noexist'), 'expects false, the page does not exist' )

        self.assertFalse( sc.get_soup_object(123), 'expects false, the url parameter can be only string type' )
        self.assertFalse( sc.get_soup_object([1,2,3]), 'expects false, the url parameter can be only string type' )

        # get_soup_object_with_auth - function used by auth site example

        # Test with right login info
        page_url  = 'http://laravel-simple-auth-example.alexpereap.com/'
        login_url = 'http://laravel-simple-auth-example.alexpereap.com/login'
        payload   = {
            "username" : "alexp",
            "password" : "Williams",
            "token"    : "_token"
            }

        self.assertTrue( sc.get_soup_object_with_auth(page_url, login_url, payload), 'expects true, login successful' )

        # Test with wrong login info
        payload   = {
            "username" : "alexp",
            "password" : "WrongPassword",
            "token"    : "_token"
            }
        
        self.assertFalse( sc.get_soup_object_with_auth(page_url, login_url, payload), 'expects true, login failed' )

        # Test with wrong page url
        page_url  = 'http://laravel-simple-auth-example.alexpereap.com/noexist'
        login_url = 'http://laravel-simple-auth-example.alexpereap.com/login'
        self.assertFalse( sc.get_soup_object_with_auth(page_url, login_url, payload), 'expects true, login failed' )

        # Test with wrong login url
        page_url  = 'http://laravel-simple-auth-example.alexpereap.com/'
        login_url = 'http://laravel-simple-auth-example.alexpereap.com/noexist'
        self.assertFalse( sc.get_soup_object_with_auth(page_url, login_url, payload), 'expects true, login failed' )
       
        
    
    def test_blog_example(self):
        be = BlogExample()

        article_expected_title = 'Brexit: Tory MPs warn of entryism threat from Leave. EU supporters'
        # sets expected article content variable from file
        f = open(self.path + '/test_expected_content.txt','r')
        article_expected_content = f.read()
        f.close()

        # get article title
        self.assertEqual(be.get_title() ,article_expected_title, 'Title equal to: ' + article_expected_title )

        # load contents
        article_contents = be.get_article_contents()
        self.assertTrue( isinstance(article_contents, str), 'article content is a string' )
        self.assertGreater( len(article_contents), 3000, 'article content is larger than 3000 chars' )
        self.assertEqual( article_contents.strip(), article_expected_content.strip(), 'article content is equal to the expected content' )
        # load images
        article_images = be.get_article_images()
        self.assertTrue( isinstance(article_images, list), 'article images is a list' )
        self.assertEqual( len(article_images), 2, 'article expects 2 images' )
    
    def test_auth_table(self):
        sa = SimpleAuthTable()
        table_expected_contents =  [
            ['Lewis Hamilton', 'Mercedes Amg Petronas Motorsport', '213'],
            ['Sebastian Vettel', 'Scuderia Ferrari', '189'], 
            ['Kimi Räikkönen', 'Scuderia Ferrari', '146'], 
            ['Valtteri Bottas', 'Mercedes Amg Petronas Motorsport', '132'], 
            ['Daniel Ricciardo', 'Aston Martin Red Bull Racing', '118'], 
            ['Max Verstappen', 'Aston Martin Red Bull Racing', '105'], 
            ['Nico Hulkenberg', 'Renault Sport Formula One Team', '52'], 
            ['Kevin Magnussen', 'Hass F1 Team', '45'], 
            ['Fernando Alonso', 'McLaren F1 Team', '44'], 
            ['Sergio Perez', 'Sahara Force India F1 Team', '30'], 
            ['Carlos Sainz', 'Renault Sport Formula One Team', '30'], 
            ['Esteban Ocon', 'Sahara Force India F1 Team', '29'], 
            ['Pierre Gasly', 'Red Bull Toro Rosso Honda', '26'], 
            ['Romain Grosjean', 'Hass F1 Team', '21'], 
            ['Charles Leclerc', 'Alfa Romeo Sauber F1 Team', '13'], 
            ['Stoffel Vandoorne', 'McLaren F1 Team', '8'], 
            ['Marcus Ericsson', 'Alfa Romeo Sauber F1 Team', '5'], 
            ['Lance Stroll', 'Williams Martini Racing', '4'], 
            ['Brendon Hartley', 'Red Bull Toro Rosso Honda', '2'], 
            ['Sergey Sirotkin', 'Williams Martini Racing', '0']
        ]

        # load table
        table = sa.get_table()
        self.assertTrue( isinstance(table, list), 'table representation is a list' )
        self.assertEqual( table, table_expected_contents, 'table is equal to expected table contents' )

        
if __name__ == '__main__':
    unittest.main()