from .Scraper import Scraper

class BlogExample(Scraper):
    """
    Class to demonstrate content scrap from example 2 from the test
    https://www.theguardian.com/politics/2018/aug/19/brexit-tory-mps-warn-of-entryism-threat-from-leave-eu-supporters
    """
    url = 'https://www.theguardian.com/politics/2018/aug/19/brexit-tory-mps-warn-of-entryism-threat-from-leave-eu-supporters'

    def __init__(self, *args, **kwargs):
        super(BlogExample, self).__init__(*args, **kwargs)
        self.soup = self.get_soup_object(self.url)
    
    def get_title(self):
        """
        Gets the website title attribute
        Returns:
        string: article title
        """
        return self.soup.title.string[:66]
    
    def get_article_contents(self):
        """
        Gets the article content
        Returns:
        string: article content
        """
        content = ''
        article_paragraphs = self.soup.find(id="article")\
                                      .find(class_='content__article-body from-content-api js-article__body')\
                                      .find_all('p')
        
        for paragrahp in article_paragraphs:
            content += "\n" + paragrahp.get_text()
        
        return content

    def get_article_images(self):
        """
        Gets the article images
        Returns:
        list: article images source
        """
        images_src = []

        article_images = self.soup.find(id='article').find_all('img')
        for x in article_images:
            images_src.append(x['src'])
        
        return images_src
